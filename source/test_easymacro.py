#!/usr/bin/env python3
# coding: utf-8

import inspect
import unittest
import easymacro as app

from com.sun.star.uno import XInterface


class BaseTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.server = app.LIBOServer()

    @classmethod
    def tearDownClass(cls):
        cls.server.stop()

    def setUp(self):
        msg = 'In method: {}'.format(self._testMethodName)
        app.debug(msg)

    # ~ @unittest.SkipTest
    def tearDown(self):
        pass


class TestPruebas(BaseTest):

    def test_new_doc(self):
        pass


class TestDocuments(BaseTest):

    def test_new_doc(self):
        result = app.new_doc()
        self.assertIsInstance(result, app.LOCalc)
        result.close()

    def test_get_type_doc(self):
        expected = 'calc'
        result = app.new_doc()
        self.assertEqual(result.type, expected)
        result.close()


class TestTools(BaseTest):

    def test_create_instance_not_exists(self):
        result = app.create_instance('no.exists')
        self.assertIsNone(result)

    def test_create_instance(self):
        result = app.create_instance('com.sun.star.frame.Desktop')
        self.assertIsNotNone(result)

    def test_set_get_config(self):
        expected = 'TEST'
        result = app.set_config('test', 'TEST', 'test')
        self.assertTrue(result)
        result = app.get_config('test', '', 'test')
        self.assertEqual(result, expected)

    def test_msgbox(self):
        expected = 0
        result = app.msgbox('TEST')
        self.assertEqual(result, expected)


class TestVars(BaseTest):

    def test_name(self):
        expected = 'LibreOffice'
        result = app.NAME
        self.assertEqual(result, expected)

    def test_version(self):
        expected = '6.2'
        result = app.VERSION
        self.assertEqual(result, expected)

    def test_lang(self):
        expected = 'en'
        result = app.LANG
        self.assertEqual(result, expected)

    def test_language(self):
        expected = 'en-US'
        result = app.LANGUAGE
        self.assertEqual(result, expected)

    def test_os(self):
        expected = 'Linux'
        result = app.OS
        self.assertEqual(result, expected)

    def test_is_win(self):
        result = app.IS_WIN
        self.assertFalse(result)

    def test_user(self):
        expected = 'mau'
        result = app.USER
        self.assertEqual(result, expected)

    def test_pc(self):
        expected = 'oficina'
        result = app.PC
        self.assertEqual(result, expected)

    def test_desktop(self):
        expected = 'gnome'
        result = app.DESKTOP
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()

