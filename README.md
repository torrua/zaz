# ZAZ

Scripts and library for develop macros in LibreOffice with Python.

Develop in pure Python, not need any dependence.

For Python 3.6+

* Look [documentation](https://gitlab.com/mauriciobaeza/zaz/wikis/home)
* Ver [documentación](https://gitlab.com/mauriciobaeza/zaz/wikis/inicio)


### Software libre, not gratis


This extension have a cost of maintenance of 1 euro every year.

BCH: `qztd3l00xle5tffdqvh2snvadkuau2ml0uqm4n875d`

BTC: `3FhiXcXmAesmQzrNEngjHFnvaJRhU1AGWV`

PayPal :(  donate ATT elmau DOT net


## Extensions develop with ZAZ

* https://gitlab.com/mauriciobaeza/zaz-barcode
* https://gitlab.com/mauriciobaeza/zaz-favorite
* https://gitlab.com/mauriciobaeza/zaz-easymacro
* https://gitlab.com/mauriciobaeza/zaz-pip
* https://gitlab.com/mauriciobaeza/zaz-inspect
